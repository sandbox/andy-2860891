<?php

/**
 * @file
 * Provides scopes to enable/disable modules based on context.
 *
 * It can be used to handle different environments, eg. dev and live, and also
 * different sites sharing a single codebase (eg. multisite, Acquia Site
 * Factory).
 *
 * It requires a config file called scope.yml in sites/all.
 *
 * Not tested on Windows.
 *
 * You can copy this file to any of the following
 * 1. A .drush folder in your HOME folder.
 * 2. Anywhere in a folder tree below an active module on your site.
 * 3. /usr/share/drush/commands (configurable)
 * 4. In an arbitrary folder specified with the --include option.
 * 5. Drupal's /drush or /sites/all/drush folders, or in the /drush folder in
 *    the directory above the Drupal root.
 *
 * It wasn't intended to be a long-lived command - I'd prefer to use an existing
 * Drupal module such as Master[1] or Environment[2], but they're not ready for
 * D8 use yet.
 *
 * [1]: https://www.drupal.org/project/master
 * [2]: https://www.drupal.org/project/environment
 *
 * @todo: Move classes into separate files and a namespace.
 * @todo: Allow scopes to include other scopes.
 * @todo: Invoke hook when scopes are enabled.
 * @todo: Make a class for handling state changes.
 * @todo: Allow other modules to alter the list of modules to disable/enable.
 * @todo: Check for exceptions, including from disabling module with content.
 */

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Implements hook_drush_command().
 */
function scope_drush_command() {
  $items = [];

  $items['scope-enable'] = [
    'description' => "Enables the specified scope(s).",
    'arguments' => [
      'scope(s)' => 'The scope(s) to enable.',
    ],
    'options' => [
      'dry-run' => [
        'description' => "Don't actually make any changes, just show what's required.",
      ]
    ],
    'examples' => [
      'drush scope-enable uk dev' => "Enable the scopes 'uk' and 'dev'.",
    ],
    'aliases' => array('se'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  ];

  $items['scope-query'] = [
    'description' => "Returns the changes required to enable the specified scope(s) in a machine-parsable format. Use 'drush scope-enable --dry-run' for human consumption.",
    'arguments' => [
      'scope(s)' => 'The scope(s) to query.',
    ],
    'examples' => [
      'drush scope-query uk dev' => "Returns the changes required to enable the 'uk' and 'dev' scopes.",
    ],
    'aliases' => array('sq'),
    'engines' => [
      'outputformat' => [
        'default' => 'json',
      ],
    ],
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  ];

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function scope_drush_help($section) {
  switch ($section) {
    case 'drush:scope-enable':
      return dt("This command will ensure only the correct modules are enabled for the given scope(s).");

    // The 'summary' meta item is displayed in `drush help --filter`,
    // and is used to give a general idea what the commands in this
    // command file do, and what they have in common.
    case 'meta:scope:summary':
      return dt("Ensures the correct modules are running in a given environment.");
  }
}

/**
 * Implements drush_hook_COMMAND_validate() for scope-enable.
 *
 * @param string[] ...$scopes
 *   Unlimited number of scope names.
 */
function drush_scope_enable_validate(...$scopes) {
  return scope_drush_validate($scopes);
}

/**
 * Implements drush_hook_COMMAND_validate() for scope-query.
 *
 * @param string[] ...$scopes
 *   Unlimited number of scope names.
 */
function drush_scope_query_validate(...$scopes) {
  return scope_drush_validate($scopes);
}

/**
 * Implements drush_hook_COMMAND() for scope-enable.
 *
 * @param string[] ...$scopes
 *   Unlimited number of scope names.
 */
function drush_scope_enable(...$scopes) {
  $dry_run = drush_get_option('dry-run');
  $config = scope_get_config();
  $installer = \Drupal::service('module_installer');
  $group_handler = new ModuleGroupHandler($config, $installer);
  $config_handler = new ConfigHandler($config, \Drupal::service('config.factory'));

  $group_handler->enableScopes($scopes, system_rebuild_module_data(), $dry_run);
  $config_handler->enableScopes($scopes, $dry_run);
}

/**
 * Implements drush_hook_COMMAND() for scope-query.
 *
 * @param string[] ...$scopes
 *   Unlimited number of scope names.
 *
 * @return array
 *   An associative array of query results.
 */
function drush_scope_query(...$scopes) {
  $config = scope_get_config();
  $installer = \Drupal::service('module_installer');
  $group_handler = new ModuleGroupHandler($config, $installer);
  $config_handler = new ConfigHandler($config, \Drupal::service('config.factory'));

  return [
    'modules' => $group_handler->queryScopes($scopes, system_rebuild_module_data()),
    'config' => $config_handler->queryScopes($scopes),
  ];
}

/**
 * Command argument complete callback.
 *
 * Provides argument values (scopes) for shell completion.
 *
 * @return string[]
 *   Array of valid scopes.
 */
function scope_scope_enable_complete() {
  return ['values' => scope_get_valid_scopes()];
}

/**
 * Return scope configuration.
 *
 * It statically caches the results, and logs an error if the file can't be
 * found.
 *
 * @return array|false
 *   The configuration array, or FALSE if the file can't be found or decoded.
 */
function scope_get_config() {
  $decoded =& drupal_static(__FUNCTION__);
  // This doesn't cache a failure on purpose so that the correct error will be
  // issued if it's called again.
  if (!$decoded) {
    $config_file = scope_get_config_file();
    if (!$config_file) {
      drush_set_error('SCOPE_MISSING_CONFIG', dt("Couldn't find the config file."));
      return FALSE;
    }
    $contents = file_get_contents($config_file);
    if ($contents === FALSE) {
      drush_set_error('SCOPE_CONFIG_READ_ERROR', dt("Couldn't read !file.", [
        '!file' => $config_file,
      ]));
      return FALSE;
    }
    try {
      $decoded = Yaml::decode($contents);
    }
    catch (Exception $e) {
      drush_set_error('SCOPE_CONFIG_PARSE_ERROR', dt("Couldn't parse the YAML in !file: !message", [
        '!file' => $config_file,
        '!message' => $e->getMessage(),
      ]));
      return FALSE;
    }
  }
  return $decoded;
}

/**
 * Validates the scopes against the config, setting a drush error on failure.
 *
 * @param string[] $scopes
 *   An indexed array of scope names
 *
 * @return bool
 *   TRUE if the scopes pass validation; FALSE otherwise.
 */
function scope_drush_validate($scopes) {
  $config = scope_get_config();
  if (!$config) {
    // An error's already logged by scope_get_config().
    return FALSE;
  }

  $missing_scopes = array_diff($scopes, array_keys($config['scopes']));
  if ($missing_scopes) {
    $message = dt("The following scopes couldn't be found: !scopes.", [
      '!scopes' => join(', ', $missing_scopes),
    ]);
    return drush_set_error('SCOPE_MISSING', $message);
  }

  return TRUE;
}

/**
 * Returns an indexed array of scopes.
 *
 * @return string[]
 */
function scope_get_valid_scopes() {
  return array_keys(scope_get_config()['scopes']);
}

/**
 * Returns the path of the config file.
 *
 * Currently it only searches in sites/all.
 *
 * @return string|false
 *   The absolute path on the local filesystem to the config file, or FALSE on
 *   failure.
 */
function scope_get_config_file() {
  $file_name = 'scope.yml';
  $dirs = ['/sites/all'];
  foreach ($dirs as $dir) {
    $file_path = DRUPAL_ROOT . $dir . "/$file_name";
    if (file_exists($file_path)) {
      return $file_path;
    }
  }
  return FALSE;
}

/**
 * Ensures the correct modules are enabled for a given scope.
 *
 * @todo: Is it nicer to pass the installer into enableScopes()?
 * @todo: Decouple from D8? from Drush?
 */
class ModuleGroupHandler {

  /**
   * ModuleGroupHandler constructor.
   *
   * @param array $config
   *   An associative array of configuration, keyed by:
   *   - module_groups: An associative array where each key is the name of a
   *     module group, and the value is an indexed array of modules that form that
   *     group.
   *   - scopes: an associative array of scope data keyed by scope name; the
   *     scope data might contain a 'module_groups' key and an array of module
   *     groups to associate with the scope.
   *
   * @param \Drupal\Core\Extension\ModuleInstallerInterface $installer
   *   The module installer to use to enable/disable modules.
   */
  public function __construct(array $config, ModuleInstallerInterface $installer) {
    $this->installer = $installer;
    $this->storeGroups($config);
    $this->parseScopes($config);
  }

  /**
   * Disable/enable modules for the specified scopes.
   *
   * @param string[] $scopes
   *   An indexed array of scopes to enable.
   * @param \Drupal\Core\Extension\Extension[] $module_data
   *   The current module data as returned by system_rebuild_module_data().
   * @param bool $dry_run
   *   (optional) If TRUE then the changes required will be displayed in human-
   *   readable form, but no action will be taken; defaults to FALSE.
   *
   * @see system_rebuild_module_data()
   */
  public function enableScopes(array $scopes, array $module_data, $dry_run = FALSE) {
    $results = $this->queryScopes($scopes, $module_data);
    if (!$results) {
      return;
    }
    $to_disable = $results['disable'];
    $to_enable = $results['enable'];

    // Early out if there's nothing to do.
    if (!$to_disable && !$to_enable) {
      return;
    }

    // Display the necessary changes.
    if ($to_disable) {
      drush_print(dt("About to uninstall !modules.", [
        '!modules' => join(', ', $to_disable),
      ]));
    }
    if ($to_enable) {
      drush_print(dt("About to install !modules.", [
        '!modules' => join(', ', $to_enable),
      ]));
    }

    // Only confirm if it's not a dry run.
    if ($dry_run || !drush_confirm(dt("Are you sure you wish to continue?"))) {
      return;
    }

    if ($to_disable) {
      $this->installer->uninstall($to_disable);
    }
    if ($to_enable) {
      $this->installer->install($to_enable);
    }
    drush_print(dt("Done"));
  }

  /**
   * Return the changes needed to enable specific scopes.
   *
   * @param string[] $scopes
   *   An indexed array of scopes to query.
   * @param \Drupal\Core\Extension\Extension[] $module_data
   *   The current module data as returned by system_rebuild_module_data().
   *
   * @return array|false
   *   An associative array keyed by:
   *   - disable: an indexed array of modules to be disabled;
   *   - enable: an indexed array of modules to be enabled;
   *   or FALSE on failure.
   *
   * @see system_rebuild_module_data()
   */
  public function queryScopes(array $scopes, array $module_data) {
    $groups = $this->getScopesGroups($scopes);
    $master_list = $this->getGroupModules($groups);
    $dependencies = $this->getDependencies($master_list, $module_data);

    if ($missing_modules = array_diff($dependencies, array_keys($module_data))) {
      // One or more of the given modules doesn't exist.
      return drush_set_error('SCOPE_MISSING_MODULE', dt("Unable to install modules !dependencies due to missing modules !missing.", [
        '!dependencies' => join(', ', $dependencies),
        '!missing' => join(', ', $missing_modules),
      ]));
    }

    $enabled = array_keys(array_filter($module_data, function ($module) {
      return $module->status;
    }));

    $to_disable = array_values(array_diff($enabled, $dependencies));
    $to_enable = array_values(array_diff($dependencies, $enabled));
    return ['disable' => $to_disable, 'enable' => $to_enable];
  }

  /**
   * Returns an indexed array of all dependencies of the given modules.
   *
   * @param string[] $modules
   *   An indexed array of modules whose dependencies should be returned;
   * @param \Drupal\Core\Extension\Extension[] $module_data
   *   The current module data as returned by system_rebuild_module_data().
   * @param bool $deduplicate
   *   (optional, internal use only) Whether to remove duplicates from the
   *   returned array.
   *
   * @return string[]
   *   An indexed array of module names without duplicates.
   *
   * @todo: Verify that there's not an existing API call to get a full set of
   *   dependencies for a module (either core or Drush).
   */
  protected function getDependencies($modules, $module_data, $deduplicate = TRUE) {
    $deps = [];
    foreach ($modules as $module) {
      $deps[] = $module;
      if (!empty($module_data[$module]->requires)) {
        $dependent_modules = $this->getDependencies(array_keys($module_data[$module]->requires), $module_data, FALSE);
        $deps = array_merge($dependent_modules, $deps);
      }
    }

    if ($deduplicate) {
      $deps = array_unique($deps);
    }

    return $deps;
  }

  /**
   * Return a unique list of modules from the groups passed.
   *
   * @param string[] $groups
   *   An indexed array of group names
   *
   * @return string[]
   *   An indexed array of module names
   */
  protected function getGroupModules($groups) {
    $modules = [];
    foreach ($groups as $group) {
      $modules = array_merge($modules, $this->groups[$group]);
    }
    return array_unique($modules);
  }

  /**
   * Return the groups for the given scopes.
   *
   * @param string[] $scopes
   *   An indexed array of scope names
   *
   * @return string[]
   *   An indexed array of groups based on the scopes passed.
   */
  protected function getScopesGroups(array $scopes) {
    $groups = [];
    foreach ($scopes as $scope) {
      if (!empty($this->scopes[$scope])) {
        $groups = array_merge($groups, $this->scopes[$scope]);
      }
    }

    return $groups;
  }

  /**
   * Store the module groups from the config.
   *
   * @param array $config
   */
  protected function storeGroups(array $config) {
    if (!empty($config['module_groups'])) {
      $this->groups = $config['module_groups'];
    }
  }

  /**
   * Parse the config for any scopes containing module groups.
   *
   * @param array $config
   */
  protected function parseScopes(array $config) {
    foreach ($config['scopes'] as $scope => $scope_data) {
      if (!empty($scope_data['module_groups'])) {
        $this->scopes[$scope] = $scope_data['module_groups'];
      }
    }
  }

  /** @var \Drupal\Core\Extension\Extension[] $moduleData */
  protected $moduleData;

  /** @var \Drupal\Core\Extension\ModuleInstallerInterface $installer */
  protected $installer;

  /**
   * An associative array keyed by group name of arrays of module names.
   *
   * @code
   * [
   *   'dev' => ['devel'],
   *   'ui' => ['features_ui', 'views_ui'],
   *   'tracking' => ['google_analytics'],
   * ]
   * @endcode
   *
   * @var array $groups
   */
  private $groups = [];

  /**
   * An associative array keyed by scope name of arrays of group names.
   *
   * @code
   * [
   *   'dev' => ['dev', 'ui'],
   *   'prod' => ['tracking'],
   * ]
   * @encode
   *
   * @var array $scopes
   */
  private $scopes = [];
}

/**
 * Ensures the correct configuration is set for a given scope.
 */
class ConfigHandler {

  /**
   * ConfigHandler constructor.
   *
   * @param array $config
   *   An associative array of configuration, keyed by:
   *   - scopes: an associative array of scope data keyed by scope name; the
   *     scope data might contain a 'config' key, under which structured config
   *     data can be passed. The keys immediately under the 'config' key should
   *     usually be bare module names.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(array $config, ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->parseScopes($config);
  }

  /**
   * Disable/enable modules for the specified scopes.
   *
   * @param string[] $scopes
   *   An indexed array of scopes to enable.
   * @param bool $dry_run
   *   (optional) If TRUE then the changes required will be displayed in human-
   *   readable form, but no action will be taken; defaults to FALSE.
   *
   * @see system_rebuild_module_data()
   */
  public function enableScopes(array $scopes, $dry_run = FALSE) {
    $config = $this->queryScopes($scopes);
    if (!$config) {
      return;
    }
    drush_print("About to change config:");
    $this->displayConfig($config);

    if ($dry_run || !drush_confirm(dt("Are you sure you wish to continue?"))) {
      return;
    }

    foreach ($config as $name => $values) {
      // Don't use Config::setData() because it completely overrides existing
      // values - we want to be able to modify just some of the values without
      // affecting others.
      $config_object = $this->configFactory->getEditable($name);
      foreach ($values as $key => $value) {
        $config_object->set($key, $value);
      }
      $config_object->save();
    }
    drush_print(dt("Done."));
  }

  /**
   * Return the changes needed to enable specific scopes.
   *
   * @param string[] $scopes
   *   An indexed array of scopes to query.
   *
   * @return array
   *   An associative array of configuration data keyed by config name, eg.
   * @code
   * [
   *   'environment_indicator.indicator' => [
   *     'bg_color' => 'darkcyan',
   *     'fg_color' => 'white',
   *     'name' => 'Local',
   *   ],
   * ]
   * @endcode
   */
  public function queryScopes(array $scopes) {
    $results = [];
    foreach ($scopes as $scope) {
      if (!empty($this->scopes[$scope])) {
        foreach ($this->scopes[$scope] as $module => $config) {
          foreach ($config as $config_name => $config_data) {
            // Check if there's any existing configuration data; only return
            // config that actually needs to change.
            $config_key = "$module.$config_name";
            $existing_config = $this->configFactory->get($config_key);
            foreach ($config_data as $name => $value) {
              if ($existing_config->get($name) !== $value) {
                $results[$config_key] = $config_data;
              }
            }
          }
        }
      }
    }

    return $results;
  }

  /**
   * Parse the config for any scopes containing they key 'config'.
   *
   * @param array $config
   */
  protected function parseScopes(array $config) {
    foreach ($config['scopes'] as $scope => $scope_data) {
      if (!empty($scope_data['config'])) {
        $this->scopes[$scope] = $scope_data['config'];
      }
    }
  }

  /**
   * Display a structured array of config values in a human-readable format.
   *
   * @param array $config
   *   The config array.
   * @param string $prefix
   *   (optional, internal use only) The prefix to prepend to scalar values on
   *   display; used to track depth inside the array.
   *
   * @todo: I wonder if there's not an existing function that does something
   *   like this.
   */
  protected function displayConfig($config, $prefix = '') {
    if (!is_array($config)) {
      drush_print("$prefix- $config");
      return;
    }

    foreach ($config as $name => $values) {
      drush_print("$prefix- $name");
      foreach ($values as $value) {
        $this->displayConfig($value, $prefix . '  ');
      }
    }
  }

  /**
   * An associative array keyed by scope name of config settings.
   *
   * The module name and config name aren't joined into a single string with a
   * dot: in the example it doesn't use 'environment_indicator.indicator' as a
   * key, even though that's what the config factory expects. The ::queryScope()
   * method handles the conversion to the dotted form.
   *
   * @code
   * [
   *   'local' => [
   *     'environment_indicator' => [
   *       'indicator' => [
   *         'bg_color' => 'darkcyan',
   *         'fg_color' => 'white',
   *         'name' => 'Local',
   *       ]
   *     ]
   *   ]
   * ]
   * @endcode
   *
   * @see ConfigHandler::queryScopes()
   *
   * @var array $scopes
   */
  protected $scopes;

  /** @var \Drupal\Core\Config\ConfigFactoryInterface $configFactory */
  protected $configFactory;
}
